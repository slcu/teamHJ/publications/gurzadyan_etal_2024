#include <iostream>                                                                                  
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <ios>
#include <algorithm>
using namespace std;


void rep(std::string& str, const std::string& from, const std::string& to) {
     if(from.empty())
         return;
     size_t start_pos = 0;
     while((start_pos = str.find(from, start_pos)) != std::string::npos) {
         str.replace(start_pos, from.length(), to);
         start_pos += to.length();
     }
}


bool is_number(std::string s) {
  s.erase(std::remove_if(s.begin(), s.end(), ::ispunct),s.end());
  s.erase(std::remove_if(s.begin(), s.end(), ::isdigit),s.end());
  s.erase(std::remove(s.begin(), s.end(), '-'), s.end());
  s.erase(std::remove(s.begin(), s.end(), 'E'), s.end());

  if(s.empty())
        return true;
  else
        return false;
}


double sciToDub(const string& str) {
   stringstream ss(str);
   double d = 0;
   ss >> d;

   if (ss.fail()) {
        string s = "Unable to format ";
        throw (s);
   }
   return (d);
}


template <typename T>
std::string NumToStr(T Number)
{
     std::ostringstream ss;
     ss << Number;
     return ss.str();
}


//linear node value list
//double values_argA[11] = {0.001, 0.501, 1.001, 1.501, 2.001, 2.501, 3.001, 3.501, 4.001, 4.501, 5.001};
//double values_argB[11] = {0.001, 1.001, 2.001, 3.001, 4.001, 5.001, 6.001, 7.001, 8.001, 9.001, 10.001};
//double values_argS1[11] = {0.001, 0.301, 0.601, 0.901, 1.201, 1.501, 1.801, 2.101, 2.401, 2.701, 3.001};
//double values_argS2[11] = {0.001, 0.701, 1.401, 2.101, 2.801, 3.501, 4.201, 4.901, 5.601, 6.301, 7.001};
//double values_argC[11] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
//double values_argP[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//double values_argQ[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


//log node value list
//double values_argA[11] = {0.001, 0.00234367, 0.0054928, 0.0128733, 0.0301709, 0.0707107, 0.165723, 0.3884, 0.910282, 2.1334, 5};    
//double values_argB[11] = {0.001, 0.00251189, 0.00630957, 0.0158489, 0.0398107, 0.1, 0.251189, 0.630957, 1.58489, 3.98107, 10};
//double values_argS1[11] = {0.001, 0.00222696, 0.00495934, 0.0110443, 0.0245951, 0.0547723, 0.121976, 0.271634, 0.604919, 1.34713, 3};
//double values_argS2[11] = {0.001, 0.00242387, 0.00587516, 0.0142406, 0.0345175, 0.083666, 0.202796, 0.491551, 1.19146, 2.88794, 7};
//double values_argC[11] = {0, 0.01, 0.0278256, 0.0774264, 0.215443, 0.599484, 1.6681, 4.64159, 12.9155, 35.9381, 100};
//double values_argP[11] = {0, 0.01, 0.0215443, 0.0464159, 0.1, 0.215443, 0.464159, 1., 2.15443, 4.64159, 10};
//double values_argQ[11] = {0, 0.001, 0.00278256, 0.00774264, 0.0215443, 0.0599484, 0.16681, 0.464159, 1.29155, 3.59381, 10};


int main()
{
	std::ifstream infile;
	std::ofstream outfile;

	std::vector<std::string> PSSlines;


	std::string FILE_NAME = "/Users/agurzadyan/NLNC_PSSLin_6Hall.txt";
//	std::string FILE_NAME = "/Users/agurzadyan/NLNC_PSSLog_2Hall.txt";

	std::string stab_outfile = "/Users/agurzadyan/src/nlnc_pss_pca/nlnc_psslin_6h_stables.txt";
	std::string unstab_outfile = "/Users/agurzadyan/src/nlnc_pss_pca/nlnc_psslin_6h_unstables.txt";
	std::string likpol_outfile = "/Users/agurzadyan/src/nlnc_pss_pca/nlnc_psslin_6h_likelypolars.txt";

	infile.open(FILE_NAME);	

	std::string str;
	while (std::getline(infile, str))
	{
    	if(str.size() > 0)
        	PSSlines.push_back(str);
	}


//	PSSlines.push_back("{4.501, 9.001, 0.001, 0.001, 0., 1., 2, 4., 5, 500},{1, 0, 0},{{-1.0000E-3, -1.0000E-3,  7.5530E-4, {}, 0E,  0E}}");	


	int linecount = 0;
	for(std::string & sLine : PSSlines)
        {
	//	std::cout<<sLine<<std::endl;
		linecount++;
	}
	std::cout<<linecount<<endl;
	infile.close();


//	std::string sLine = "{0.501, 10.001, 2.401, 0.001, 10., 10., 2, 0., 5, 500},{1, 1, 1},{{ 2.5650E-1,  1.6110E0,  3.4650E0, { 9.6740E-1,  1.0270E1},  0E,  1.9450E0}}";
	
	for(std::string & sLine : PSSlines)
        {
	sLine.erase(std::remove(sLine.begin(), sLine.end(), '{'), sLine.end());
	sLine.erase(std::remove(sLine.begin(), sLine.end(), '}'), sLine.end());
	sLine.erase(std::remove(sLine.begin(), sLine.end(), ' '), sLine.end());
	rep(sLine,",,",",");
	
	size_t pos = 0; 
	std::string token; 
	std::vector<double> parsed;
	while ((pos = sLine.find(",")) != std::string::npos) { 
	  token = sLine.substr(0, pos); 
	    
		if (token.back()=='E') token += '0'; 

//      	std::cout << token << std::endl; 

		try { 
			parsed.push_back(sciToDub(token)); 
		}   
		catch (string& e) { 
//     		         cerr << "Whoops: " << e << endl; 
		}   


		sLine.erase(0, pos + 1); 
	}   
	try { 
		parsed.push_back(sciToDub(sLine+'0')); 
	}   
	catch (string& e) { 
//              cerr << "Whoops: " << e << endl; 
	}   

//	for (size_t i=0; i<parsed.size();++i)
//		std::cout<<parsed[i]<<",";
//	std::cout<<std::endl;
	
	
	if(parsed[11]==0) {
	outfile.open(stab_outfile, ios::out | ios::app);
		for(size_t i = 0; i < 8; ++i)
			outfile << parsed[i] << ",";
		outfile << parsed[10] << endl;
		outfile.close();
	}
	

	if(parsed[11]>0 && parsed[11]-parsed[12]==0) {
	outfile.open(unstab_outfile, ios::out | ios::app);
		for(size_t i = 0; i < 8; ++i)
			outfile << parsed[i] << ",";
		outfile << parsed[10] << endl;
		outfile.close();
	}

	if(parsed[11]-parsed[12]>0) {
		outfile.open(likpol_outfile, ios::out | ios::app);
		for(size_t i = 0; i < 8; ++i)
			outfile << parsed[i] << ",";
		outfile << parsed[10] << endl;
		outfile.close();
	}

	}


	return 0;
}
