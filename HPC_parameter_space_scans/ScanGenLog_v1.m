(* ::Package:: *)

(*Limits Mathematica to requested resources*)
Unprotect[$ProcessorCount];$ProcessorCount = 50;
 
(*Prints the machine name that each kernel is running on*)
Print[ParallelEvaluate[$MachineName]];


(*Genralised parameter space scan output stream function for NLNC Active-Inactive ROP model*)
ScanGenFooMstream[ID_,filename_,ARG_]:=Module[{jacob,f,gu,gv,eqs,lapl,steadies,scanstream},
a=ARG[[1]];b=ARG[[2]];r1=ARG[[3]];r2=ARG[[4]];c=ARG[[5]];p=ARG[[6]];h=ARG[[7]];q=ARG[[8]];Dif1=ARG[[9]];Dif2=ARG[[10]];
Quiet[f[u_,v_]:=v*(c*u^h/(p^h+u^h))+v*q;
LL=100;
gu[u_,v_]:=f[u,v]-u-r1*u+a;
gv[u_,v_]:=-f[u,v]+u-r2*v+b;
eqs={gu[u,v],gv[u,v]};vars={u,v};
lapl[Du_,Dv_]:=((2\[Pi]/(LL))^2)DiagonalMatrix[{Du k^2,Dv k^2}];
steadies=N[Solve[Rationalize[{gu[u,v]==0,gv[u,v]==0,u>=0,v>=0}],{u,v},Reals,Cubics->True,Quartics->True]];
usteadies=u/.steadies;
vsteadies=v/.steadies;
jacob=D[eqs,{vars}]-lapl[Dif1,Dif2];



WriteString[OpenAppend[filename],ToString[{ARG}]<>","<>ToString[
NumberForm[{
(*0 for the total number of steady states*)
If[MemberQ[ID,0],Length[steadies]],
(*1 for the number of unstable states in any mode*)
If[MemberQ[ID,1],Length[Select[MaxValue[Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]],k]&/@Range[Length[steadies]],Positive]]],
(*2 for the number of unstable states in homogeneous mode,ie k=0*)
If[MemberQ[ID,2],Length[Select[Max[Re[Eigenvalues[(D[eqs,{vars}])/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]&/@Range[Length[steadies]],Positive]]],

(*3 for the maximal unstable wave-number for all unstable states*)
If[MemberQ[ID,3],Select[If[MaxValue[Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]],k]>0,Quiet[ArgMax[{k,Reduce[{Max[Re[Eigenvalues[jacob/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]>0,k>=0},k]},k]]]&/@Range[Length[steadies]],Positive]],
(*4 for the maximal wave-number when the state is stable in k=0 and unstable for some k>0*)
If[MemberQ[ID,4],Max[Select[Quiet[ArgMax[{k,Reduce[{Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]>0,Max[Re[Eigenvalues[D[eqs,{vars}]/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]<=0,k>=0},k]},k]&/@Range[Length[steadies]]],Positive]]],
(*5 for wave-number of the \[Lambda]max when the the state is stable in k=0 and instable for some k>0*)
If[MemberQ[ID,5],Max[Select[If[MaxValue[Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]],k]>0&&Max[Re[Eigenvalues[(D[eqs,{vars}])/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]<0,Abs[ArgMax[Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]],k]]]&/@Range[Length[steadies]],Positive]]],
(*6 ALL wave-numbers of \[Lambda]max and \[Lambda]max themselves*)
If[MemberQ[ID,6],FindMaximum[{Max[Re[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]],k>=0},k]&/@Range[Length[steadies]]],
(*7 ALL wave-number solutions for \[Lambda]=0*)
If[MemberQ[ID,7],Quiet[NSolve[{Max[Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]==0,k>=0},k]][[All,1,2]]&/@Range[Length[steadies]]],
(*8 for boolean indicator of non-zero imaginary part of all maximal eigenvalues*)
If[MemberQ[ID,8],If[Max[Im[Eigenvalues[(D[eqs,{vars}])/.{u->usteadies[[#]],v->vsteadies[[#]]}]]]>0,1,0]&/@Range[Length[steadies]]]}
,{4,4},NumberSigns->{"-"," "},ExponentFunction->(#&),NumberFormat->(Row[{#1,"E",#3}]&)]]<>"\n"]];
Close[filename];];





(*Genralised parameter space scan output stream function for NLNC Active-Inactive ROP model*)
ScanGenFooMstreamNew[ID_,filename_,ARG_]:=Module[{jacob,f,gu,gv,eqs,lapl,steadies,scanstream},
a=ARG[[1]];b=ARG[[2]];r1=ARG[[3]];r2=ARG[[4]];c=ARG[[5]];p=ARG[[6]];h=ARG[[7]];q=ARG[[8]];Dif1=ARG[[9]];Dif2=ARG[[10]];
Quiet[
f[u_,v_]:=v*(c*u^h/(p^h+u^h))+v*q;
LL=100;
gu[u_,v_]:=f[u,v]-u-r1*u+a;
gv[u_,v_]:=-f[u,v]+u-r2*v+b;
eqs={gu[u,v],gv[u,v]};vars={u,v};
lapl[Du_,Dv_]:=((2\[Pi]/(LL))^2)DiagonalMatrix[{Du k^2,Dv k^2}];
steadies=N[Solve[Rationalize[{gu[u,v]==0,gv[u,v]==0,u>=0,v>=0}],{u,v},Reals,Cubics->True,Quartics->True]];
usteadies=u/.steadies;
vsteadies=v/.steadies;
jacob=D[eqs,{vars}]-lapl[Dif1,Dif2];

eigvk0 = Eigenvalues[(D[eqs,{vars}])/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]&/@Range[Length[steadies]]; 
eigvkmax = FindMaximum[{Max[Re[Eigenvalues[(jacob) /. {u -> usteadies[[#]], v -> vsteadies[[#]]}, Cubics -> True, Quartics -> True]]], k >= 0}, k]&/@Range[Length[steadies]];
eigv=Eigenvalues[(jacob)/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]&/@Range[Length[steadies]];

(*kmaxnonhom = Max[Select[Quiet[ArgMax[{k,Reduce[{Max[Re[eigv[[#]]]]>0,Max[Re[Eigenvalues[D[eqs,{vars}]/.{u->usteadies[[#]],v->vsteadies[[#]]},Cubics->True,Quartics->True]]]<=0,k>=0},k]},k]&/@Range[Length[steadies]]],Positive]];*)
klmaxnonhom = Select[If[MaxValue[Max[Re[eigv[[#]]]],k]>0&&Max[Re[eigvk0[[#]]]]<0,Abs[ArgMax[Max[Re[eigv[[#]]]],k]],0]&/@Range[Length[steadies]],NonNegative];

WriteString[OpenAppend[filename],ToString[ARG]<>","<>ToString[{Length[steadies],
Length[Select[MaxValue[Max[Re[eigv[[#]]]],k]&/@Range[Length[steadies]],Positive]],
Length[Select[Max[Re[eigvk0[[#]]]]&/@Range[Length[steadies]],Positive]]}]<>","<>ToString[
NumberForm[{Max[Re[eigvk0][[#]]],eigvkmax[[#]][[1]],eigvkmax[[#]][[2,1,2]],NSolve[{Max[eigv[[#]]]==0,k>=0},k][[All,1,2]], klmaxnonhom[[#]],Max[Im[eigvk0][[#]]]}&/@Range[Length[steadies]]
,{4,4},NumberSigns->{"-"," "},ExponentFunction->(#&),NumberFormat->(Row[{#1,"E",#3}]&)]]<>"\n"]];
Close[filename];];


(*Define the parameter ranges*)
fSpace[min_, max_, steps_, f_: Log10] := 
 10^#1 & /@ Range[f@min, f@max, (f@max - f@min)/(steps - 1)]


stepqty = 10;

comlineargs=ToExpression[$ScriptCommandLine[[2;;All]]];

argA = {N[fSpace[0.001, 5, stepqty + 1]][[comlineargs[[1]]]]};
argB = {N[fSpace[0.001, 10, stepqty + 1]][[comlineargs[[2]]]]};

argS1 = N[fSpace[0.001, 3, stepqty + 1]];
argS2 = N[fSpace[0.001, 7, stepqty + 1]];

argC = Join[{0}, fSpace[0.01, 100, stepqty]];
argP = Join[{0}, fSpace[0.01, 10, stepqty]];
argH = Range[6, 6, 1];
argQ = Join[{0}, fSpace[0.001, 10, stepqty]];

argDIFF1 = Range[5, 5, 1];
argDIFF2 = Range[500, 500, 1];


(*Print the number of nodes in the parameter space*)
Print["Running for A="<>ToString[argA[[1]]]<>", B="<>ToString[argB[[1]]]<>". Parameter space node qty="<>ToString[Length[argA]*Length[argB]*Length[argS1]*Length[argS2]*Length[argC]*Length[argP]*Length[argH]*Length[argQ]*Length[argDIFF1]*Length[argDIFF2]]<>"\n"];

(*Define the stream into the output file*)
filename="/home/ag979/PSS/NLNC_PSSLog_"<>ToString[argH[[1]]]<>"H_"<>ToString[comlineargs[[1]]]<>"_"<>ToString[comlineargs[[2]]]<>".txt";
If[FileExistsQ[filename],DeleteFile[filename]];
CreateFile[filename];



(*LoopRun*)
Print["The parameter loop scan has started at "<>DateString["ISODateTime"]<>"\n"];
Print[Timing[ParallelDo[ScanGenFooMstreamNew[{0,1,2,3,4,5,6,7,8},filename,{iargA,iargB,iargS1,iargS2,iargC,iargP,iargH,iargQ,iargDIFF1,iargDIFF2}],
{iargA,argA},
{iargB,argB},
{iargS1,argS1},
{iargS2,argS2},
{iargC,argC},
{iargP,argP},
{iargH,argH},
{iargQ,argQ},
{iargDIFF1,argDIFF1},
{iargDIFF2,argDIFF2}
]]]

