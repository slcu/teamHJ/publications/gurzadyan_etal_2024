#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <ios>
#include <algorithm>
using namespace std;


void rep(std::string& str, const std::string& from, const std::string& to) {
     if(from.empty())
         return;
     size_t start_pos = 0;
     while((start_pos = str.find(from, start_pos)) != std::string::npos) {
         str.replace(start_pos, from.length(), to);
         start_pos += to.length();
     }
}


bool is_number(std::string s) {
  s.erase(std::remove_if(s.begin(), s.end(), ::ispunct),s.end());  
  s.erase(std::remove_if(s.begin(), s.end(), ::isdigit),s.end());  
  s.erase(std::remove(s.begin(), s.end(), '-'), s.end());
  s.erase(std::remove(s.begin(), s.end(), 'E'), s.end());
 
  if(s.empty())
	return true;
  else 
	return false;
}


double sciToDub(const string& str) {
   stringstream ss(str);
   double d = 0;
   ss >> d;

   if (ss.fail()) {
	string s = "Unable to format ";
	throw (s);
   }
   return (d);
}



int main()
{
 
 std::string FILE_NAME;
 std::string OFILE_NAME; 

for (int index = 1; index<=3; index++) {

    if ( index == 1 ) {
      FILE_NAME = "/home/ag979/PSS/ParScanGenLog2Hcomplete";
      OFILE_NAME = "/home/ag979/APSS/ParScanGenLog2Hcomplete";
    }
    if ( index == 2 ) {
      FILE_NAME = "/home/ag979/PSS/ParScanGenLog4Hcomplete"; 
      OFILE_NAME = "/home/ag979/APSS/ParScanGenLog4Hcomplete";
    }
    if (index == 3 ) {
      FILE_NAME = "/home/ag979/PSS/ParScanGenLog6Hcomplete";
      OFILE_NAME = "/home/ag979/APSS/ParScanGenLog6Hcomplete";
    }
  
  
    std::ifstream infile(FILE_NAME + ".txt");

// 1 to export parameters + number of HSS + number of UHSS + number of UHSS at k=0
// 2 to export parameters + all parsed numbers
// 3 to export parameters + wavelength of the maximal eigenvalue (range near 1 is specified)
// 4 to export parsed files with subtle conditions: stable at k=0, unstable at k>0 and k_lambda_max is near 1 
// 5 to export parsed files with subtle conditions: on \lambda_max, instability in k=0 mode, etc.
 

    const int FLAG = 2;




    std::ofstream ost; 
    std::ofstream ostkneq1;
    std::ofstream ostkeq1;
    std::ofstream oststab;

	if ( FLAG == 1 || FLAG == 2 )
		ost.open(FILE_NAME + ".csv");
	if ( FLAG == 3 ) {
		ostkneq1.open(OFILE_NAME + "kneq1.csv");
		ostkeq1.open(OFILE_NAME + "keq1.csv");
		oststab.open(OFILE_NAME + "stab.csv");
	}
	if ( FLAG == 4 ) {
		ostkneq1.open(OFILE_NAME + "unst_subtle.csv");
		ostkeq1.open(OFILE_NAME + "pol_subtle.csv");
		oststab.open(OFILE_NAME + "st_subtle.csv");
	}
	if ( FLAG == 5 ) {
		ostkneq1.open(OFILE_NAME + "LLunst_subtle.csv");
		ostkeq1.open(OFILE_NAME + "LLpol_subtle.csv");
		oststab.open(OFILE_NAME + "LLst_subtle.csv");
	}
	if ( FLAG == 6 ) {
		ostkneq1.open(OFILE_NAME + "2UHSSkq1.csv");
		ostkeq1.open(OFILE_NAME + "?.csv");
		oststab.open(OFILE_NAME + "??.csv");
	}





  std::string sLine;
  std::vector<double> parsed;
  size_t NUM_OF_LINES = -1; 
//  size_t num = 0;


// Get the number of lines in file
  while(!infile.eof()) {
//	std::cout<<"Counting"<<std::endl;
	getline(infile, sLine);
	NUM_OF_LINES++;
  }

  infile.clear();
  infile.seekg(0, ios::beg);

  std::cout<<NUM_OF_LINES<<std::endl;




  if (infile.good())  {
//	std::cout<<"in"<<endl;
//	while(!infile.eof())  {
	for (size_t num = 0; num < NUM_OF_LINES ; ++num)  {

 

		std::string sLine;
		std::vector<double> parsed;
 
		std::getline(infile,sLine); 
		sLine.erase(std::remove(sLine.begin(), sLine.end(), '{'), sLine.end());
		sLine.erase(std::remove(sLine.begin(), sLine.end(), '}'), sLine.end());
		rep(sLine," -> ","=");
		sLine.erase(std::remove(sLine.begin(), sLine.end(), ' '), sLine.end());
		rep(sLine,",,",",");
		rep(sLine,",,",",");
		rep(sLine,",,",",");

		rep(sLine,"k=","");
	
//		std::cout << sLine << std::endl;


		size_t pos = 0;
		std::string token;
		while ((pos = sLine.find(",")) != std::string::npos) {
  		  token = sLine.substr(0, pos);
		
		if (token.back()=='E') token += '0';

//		std::cout << token << std::endl;
  
		try {
			parsed.push_back(sciToDub(token));
		}
		catch (string& e) {
//			cerr << "Whoops: " << e << endl;
		}


		sLine.erase(0, pos + 1);
		}
		try {
			parsed.push_back(sciToDub(sLine+'0'));
		}
		catch (string& e) {
//			cerr << "Whoops: " << e << endl;
		}

	

/*  		if (is_number(token))
		{ 	
			std::cout<<token<<std::endl;
			parsed.push_back(std::stod(token));
		}
		sLine.erase(0, pos + 1);
		}
//		std::cout<<sLine<<std::endl;
		if (is_number(sLine)) 
			parsed.push_back(std::stod(sLine));
*/

//		for (size_t i=0; i<parsed.size();++i)
//			std::cout<<parsed[i]<<",";
//		std::cout<<std::endl;
	if (FLAG == 1) {
		ost << parsed[0]<<",";
		ost << parsed[1]<<",";
		ost << parsed[2]<<",";
		ost << parsed[3]<<",";
		ost << parsed[4]<<",";
		ost << parsed[5]<<",";
		ost << parsed[6]<<",";
		ost << parsed[7]<<",";
		ost << parsed[8]<<",";
		ost << parsed[9]<<",";
		ost << parsed[10]<<",";
		ost << parsed[11]<<",";
		ost << parsed[12]<<",";
		ost << parsed[parsed.size()-1]<<std::endl;
	}

	if ( FLAG == 2 ) {
		for (size_t i=0; i<parsed.size()-1;++i)
			ost<<parsed[i]<<",";
		ost<<parsed[parsed.size()-1]<<std::endl;
	}

	if ( FLAG == 3 ) {
		if (parsed.size()>1 && parsed[11]>0)  {
			int klambmaxind = 17;
			if (parsed[12]>0 && parsed[11] == parsed[12])
				klambmaxind-=2;
			if (parsed[klambmaxind-1]<0 && parsed.size() >= klambmaxind+2)
				klambmaxind+=2;
			if (parsed[klambmaxind-1]<0 && parsed[11] == parsed[12] && parsed.size()<klambmaxind+2)
				klambmaxind-=2;
			if (parsed[10]>1 && parsed[klambmaxind + 1]>0 && parsed[klambmaxind+2]>parsed[klambmaxind]) 
				klambmaxind+=2;
//specify the desired region
//                      if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.5)   
			if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.2)   
			{
				ostkneq1 << parsed[0]<<",";
				ostkneq1 << parsed[1]<<",";
				ostkneq1 << parsed[2]<<",";
				ostkneq1 << parsed[3]<<",";
				ostkneq1 << parsed[4]<<",";
				ostkneq1 << parsed[5]<<",";
				ostkneq1 << parsed[6]<<",";
				ostkneq1 << parsed[7]<<std::endl;
			}
			else
			{
				ostkeq1 << parsed[0]<<",";
				ostkeq1 << parsed[1]<<",";
				ostkeq1 << parsed[2]<<",";
				ostkeq1 << parsed[3]<<",";
				ostkeq1 << parsed[4]<<",";
				ostkeq1 << parsed[5]<<",";
				ostkeq1 << parsed[6]<<",";
				ostkeq1 << parsed[7]<<std::endl;
			}
		}
	
		if (parsed.size()>1 && parsed[11]==0)
		{
			oststab << parsed[0]<<",";
			oststab << parsed[1]<<",";
			oststab << parsed[2]<<",";
			oststab << parsed[3]<<",";
			oststab << parsed[4]<<",";
			oststab << parsed[5]<<",";
			oststab << parsed[6]<<",";
			oststab << parsed[7]<<std::endl;
		}
	}
             
	if ( FLAG == 4 ) {
		if (parsed.size()>1 && parsed[11]>0)  {
			int klambmaxind = 17;
			if (parsed[12]>0 && parsed[11] == parsed[12])
				klambmaxind-=2;
			if (parsed[klambmaxind-1]<0 && parsed.size() >= klambmaxind+2)
				klambmaxind+=2;
			if (parsed[klambmaxind-1]<0 && parsed[11] == parsed[12] && parsed.size()<klambmaxind+2)
				klambmaxind-=2;
			if (parsed[10]>1 && parsed[klambmaxind + 1]>0 && parsed[klambmaxind+2]>parsed[klambmaxind]) 
				klambmaxind+=2;
//specify the desired region
//                      if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.5)   
			if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.2)   
			{
				ostkneq1 << parsed[0]<<",";
				ostkneq1 << parsed[1]<<",";
				ostkneq1 << parsed[2]<<",";
				ostkneq1 << parsed[3]<<",";
				ostkneq1 << parsed[4]<<",";
				ostkneq1 << parsed[5]<<",";
				ostkneq1 << parsed[6]<<",";
				ostkneq1 << parsed[7]<<std::endl;
			}
			else
			{    
				if(parsed[12]<parsed[11]) {
					ostkeq1 << parsed[0]<<",";
					ostkeq1 << parsed[1]<<",";
					ostkeq1 << parsed[2]<<",";
					ostkeq1 << parsed[3]<<",";
					ostkeq1 << parsed[4]<<",";
					ostkeq1 << parsed[5]<<",";
					ostkeq1 << parsed[6]<<",";
					ostkeq1 << parsed[7]<<std::endl;	}
				else {
					ostkneq1 << parsed[0]<<",";
					ostkneq1 << parsed[1]<<",";
					ostkneq1 << parsed[2]<<",";
					ostkneq1 << parsed[3]<<",";
					ostkneq1 << parsed[4]<<",";
					ostkneq1 << parsed[5]<<",";
					ostkneq1 << parsed[6]<<",";
					ostkneq1 << parsed[7]<<std::endl;	}


			}
		}
	
		if (parsed.size()>1 && parsed[11]==0)
		{
			oststab << parsed[0]<<",";
			oststab << parsed[1]<<",";
			oststab << parsed[2]<<",";
			oststab << parsed[3]<<",";
			oststab << parsed[4]<<",";
			oststab << parsed[5]<<",";
			oststab << parsed[6]<<",";
			oststab << parsed[7]<<std::endl;
		}
	}
                          
	if ( FLAG == 5 ) {
		if (parsed.size()>1 && parsed[11]>0)  {
			int klambmaxind = 17;
			if (parsed[12]>0 && parsed[11] == parsed[12])
				klambmaxind-=2;
			if (parsed[klambmaxind-1]<0 && parsed.size() >= klambmaxind+2)
				klambmaxind+=2;
			if (parsed[klambmaxind-1]<0 && parsed[11] == parsed[12] && parsed.size()<klambmaxind+2)
				klambmaxind-=2;
			if (parsed[10]>1 && parsed[klambmaxind + 1]>0 && parsed[klambmaxind+2]>parsed[klambmaxind]) 
				klambmaxind+=2;
//specify the desired region
//                      if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.5)   
			if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.2)   
			{
				ostkneq1 << parsed[0]<<",";
				ostkneq1 << parsed[1]<<",";
				ostkneq1 << parsed[2]<<",";
				ostkneq1 << parsed[3]<<",";
				ostkneq1 << parsed[4]<<",";
				ostkneq1 << parsed[5]<<",";
				ostkneq1 << parsed[6]<<",";
				ostkneq1 << parsed[7]<<std::endl;
			}
			else
			{    
				if(parsed[klambmaxind-1] < 2) {
					ostkeq1 << parsed[0]<<",";
					ostkeq1 << parsed[1]<<",";
					ostkeq1 << parsed[2]<<",";
					ostkeq1 << parsed[3]<<",";
					ostkeq1 << parsed[4]<<",";
					ostkeq1 << parsed[5]<<",";
					ostkeq1 << parsed[6]<<",";
					ostkeq1 << parsed[7]<<std::endl;	}
				else {
					ostkneq1 << parsed[0]<<",";
					ostkneq1 << parsed[1]<<",";
					ostkneq1 << parsed[2]<<",";
					ostkneq1 << parsed[3]<<",";
					ostkneq1 << parsed[4]<<",";
					ostkneq1 << parsed[5]<<",";
					ostkneq1 << parsed[6]<<",";
					ostkneq1 << parsed[7]<<std::endl;	}


			}
		}
	
		if (parsed.size()>1 && parsed[11]==0)
		{
			oststab << parsed[0]<<",";
			oststab << parsed[1]<<",";
			oststab << parsed[2]<<",";
			oststab << parsed[3]<<",";
			oststab << parsed[4]<<",";
			oststab << parsed[5]<<",";
			oststab << parsed[6]<<",";
			oststab << parsed[7]<<std::endl;
		}
	}
             
	if ( FLAG == 6 ) {
		if (parsed.size()>1 && parsed[11]>0)  {
			int klambmaxind = 17;
			if (parsed[12]>0 && parsed[11] == parsed[12])
				klambmaxind-=2;
			if (parsed[klambmaxind-1]<0 && parsed.size() >= klambmaxind+2)
				klambmaxind+=2;
			if (parsed[klambmaxind-1]<0 && parsed[11] == parsed[12] && parsed.size()<klambmaxind+2)
				klambmaxind-=2;
			if (parsed[10]>1 && parsed[klambmaxind + 1]>0 && parsed[klambmaxind+2]>parsed[klambmaxind]) 
				klambmaxind+=2;
//specify the desired region
//                      if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.5)   
			if (parsed[klambmaxind] < 0.8 || parsed[klambmaxind] > 1.2)   
			{
				ostkneq1 << parsed[0]<<",";
				ostkneq1 << parsed[1]<<",";
				ostkneq1 << parsed[2]<<",";
				ostkneq1 << parsed[3]<<",";
				ostkneq1 << parsed[4]<<",";
				ostkneq1 << parsed[5]<<",";
				ostkneq1 << parsed[6]<<",";
				ostkneq1 << parsed[7]<<std::endl;
			}
			else
			{    
				if(parsed[12]<parsed[11]) {
					ostkeq1 << parsed[0]<<",";
					ostkeq1 << parsed[1]<<",";
					ostkeq1 << parsed[2]<<",";
					ostkeq1 << parsed[3]<<",";
					ostkeq1 << parsed[4]<<",";
					ostkeq1 << parsed[5]<<",";
					ostkeq1 << parsed[6]<<",";
					ostkeq1 << parsed[7]<<std::endl;	}
				else {
					ostkneq1 << parsed[0]<<",";
					ostkneq1 << parsed[1]<<",";
					ostkneq1 << parsed[2]<<",";
					ostkneq1 << parsed[3]<<",";
					ostkneq1 << parsed[4]<<",";
					ostkneq1 << parsed[5]<<",";
					ostkneq1 << parsed[6]<<",";
					ostkneq1 << parsed[7]<<std::endl;	}


			}
		}
	
		if (parsed.size()>1 && parsed[11]==0)
		{
			oststab << parsed[0]<<",";
			oststab << parsed[1]<<",";
			oststab << parsed[2]<<",";
			oststab << parsed[3]<<",";
			oststab << parsed[4]<<",";
			oststab << parsed[5]<<",";
			oststab << parsed[6]<<",";
			oststab << parsed[7]<<std::endl;
		}
	}
          
             
//		num++;
	}
//	std::cout<<"Number of lines: "<<num<<std::endl;
  }

  infile.close();

  if (FLAG == 1 || FLAG == 2)  ost.close();
  if (FLAG == 3 || FLAG == 4 || FLAG == 5) {
  ostkneq1.close();
  ostkeq1.close();
  oststab.close();
  }

std::cout<<index<<"  is completed."<<std::endl;
}

std::cout<<"Press key";
std::cin.get();

}
