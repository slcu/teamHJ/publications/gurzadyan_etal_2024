(* ::Package:: *)

(*Limits Mathematica to requested resources*)
Unprotect[$ProcessorCount];$ProcessorCount = 50;
 
(*Prints the machine name that each kernel is running on*)
Print[ParallelEvaluate[$MachineName]];

(*Leading power spectra and number of peaks based on averaged MinMax algorithm*)
FsolveLPSAMM[pararray_,filename_,compnum_]:=Module[{ux,vx,uxdata,vxdata,uxdataALL,vxdataALL,steadysoln,L},
f[u_,v_]:=v*(c*u^h/(p^h+u^h))+v*q;
gu[u_,v_]:=f[u,v]-u-r1*u+a;
gv[u_,v_]:=-f[u,v]+u-r2*v+b;

a=pararray[[1]];b=pararray[[2]];r1=pararray[[3]];r2=pararray[[4]];c=pararray[[5]];p=pararray[[6]];h=pararray[[7]];q=pararray[[8]];
Du=pararray[[9]];Dv=pararray[[10]];
L=compnum;

(*final simulation time*)
tfin=500;

(*initial conditions*)
steadysoln=N[Quiet[Solve[Rationalize[{gu[u,v]==0,gv[u,v]==0,u>= 0,v>= 0}],{u,v},Reals,Cubics->True,Quartics->True]]];

(*kick generation in the initial conditions*)
kickstart=50; (*kick starts here*)
kickend=90; (*kick ends here*)
\[Delta]u=0u/.steadysoln[[1]];(*kick amplitude*)
\[Delta]v=0v/.steadysoln[[1]];(*kick amplitude*)

uxdataALL={};vxdataALL={};
For[hssnum=1,hssnum<=Length[steadysoln],hssnum++,
ind=Table[i-1,{i,L+3}];
ind[[1]]=L;ind[[L+2]]=1;ind[[L+3]]=2;
cc[i_]:=ind[[i+1]];
ux=Table[ToExpression[StringJoin["u",ToString[i]]],{i,1,L}];vx=Table[ToExpression[StringJoin["v",ToString[i]]],{i,1,L}];
usteadysoln=u/.steadysoln[[hssnum]];vsteadysoln=v/.steadysoln[[hssnum]];
Table[ukick[i]=usteadysoln+(If[kickstart<=i<=kickend&&\[Delta]u!=0,\[Delta]u-usteadysoln,0]),{i,1,L}];
Table[vkick[i]=vsteadysoln+(If[kickstart<=i<=kickend&&\[Delta]v!=0,\[Delta]v-vsteadysoln,0]),{i,1,L}];
evolu[u_,um1_,uM1_,v_]:=gu[u,v]+Du*(uM1+um1-2*u);evolv[u_,v_,vm1_,vM1_]:=gv[u,v]+Dv*(vM1+vm1-2*v);
ss=NDSolve[Flatten[
Table[{ux[[i]]'[t]==evolu[ux[[cc[i]]][t],ux[[cc[i-1]]][t],ux[[cc[i+1]]][t],vx[[i]][t]],
vx[[i]]'[t]==evolv[ux[[cc[i]]][t],vx[[cc[i]]][t],vx[[cc[i-1]]][t],vx[[cc[i+1]]][t]],
ux[[i]][0]==ukick[i](1+0.1 RandomVariate[UniformDistribution[{0,1}],1,WorkingPrecision->10][[1]]),vx[[i]][0]==vkick[i](1+0.1RandomVariate[UniformDistribution[{0,1}],1,WorkingPrecision->10][[1]])},{i,1,L}]],
Flatten[Table[{ux[[i]],vx[[i]]},{i,1,L}]],{t,0,tfin},Method->"StiffnessSwitching"];
jump=10;
uxdata=Table[Flatten[Table[{i,ux[[i]][kk]}/.ss,{i,1,L}],1],{kk,0,tfin,jump}];vxdata=Table[Flatten[Table[{i,vx[[i]][kk]}/.ss,{i,1,L}],1],{kk,0,tfin,jump}];
AppendTo[uxdataALL,uxdata];AppendTo[vxdataALL,vxdata];
];peakN=ConstantArray[0,Length[steadysoln]];
(minmax=(Max[uxdataALL[[#,tfin/jump,All,2]]]+Min[uxdataALL[[#,tfin/jump,All,2]]])/2;crossedfrombelow=0;
For[i=1,i<=Length[uxdataALL[[#,tfin/jump,All,2]]],i++,
If[uxdataALL[[#,tfin/jump,All,2]][[i]]>minmax&&crossedfrombelow== 0,crossedfrombelow=1];
If[uxdataALL[[#,tfin/jump,All,2]][[i]]<minmax&&crossedfrombelow== 1,crossedfrombelow=0;peakN[[#]]++];
];)&/@Range[1,Length[steadysoln]];
WriteString[OpenAppend[filename],ToString[Du]<>","<>ToString[Dv]<>","<>ToString[L]<>","<>ToString[NumberForm[Abs[Fourier[uxdataALL[[#, tfin/jump, All, 2]] - uxdataALL[[#, 1, All, 2]]]]^2 & /@ Table[i, {i, 1, Length[steadysoln]}], {4, 4}, NumberSigns -> {"-", " "}, ExponentFunction -> (# &), NumberFormat -> (Row[{#1, "E", #3}] &)]]<>","<>ToString[peakN]<>"\n"];
Close[filename];
]


global = {0.6, 0.7, 0.45, 0.13, 3, 3, 6, 0.5};

argDIFF1 = {0.01,0.05,0.1,0.5,1,5,10,50,100,200,300,400,500};
argDIFF2 = {50,500,5000,50000,250000,500000,2500000,5000000,50000000};
argL = {100,250,500,1000};


(*Print the number of nodes in the parameter space*)
Print["Total number of simulations = "<>ToString[Length[argL]*Length[argDIFF1]*Length[argDIFF2]]<>".\n"];

(*Define the stream into the output file*)
filename="/home/ag979/Scale/ScalePowerSpectra/ScaleProbePrSpV7_"<>DateString["ISODateTime"]<>".txt";
If[FileExistsQ[filename],DeleteFile[filename]];
CreateFile[filename];
WriteString[OpenAppend[filename],"State parameters {a,b,r1,r2,c,p,h,q} = "<>ToString[global[[1;;8]]]<>"\n"];


(*LoopRun*)
Print["The parallel code has started at "<>DateString["ISODateTime"]<>".\n"];
ParallelDo[
FsolveLPSAMM[{global[[1]],global[[2]],global[[3]],global[[4]],global[[5]],global[[6]],global[[7]],global[[8]],iargDIFF1,iargDIFF2},filename,iargL],
{iargDIFF1,argDIFF1},
{iargDIFF2,argDIFF2},
{iargL,argL}
];
Print["The parallel code has finished at "<>DateString["ISODateTime"]<>".\n"];




