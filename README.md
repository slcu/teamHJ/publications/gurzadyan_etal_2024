# Source codes and datasets for Gurzadyan et al, 2024

## Introduction

This repository holds files for the computer algebra analysis, model simulations, parallelised simulations on the High Performance Computer, parameter space scans on the High Performance Computer, data parsing and plot visulalisation for the manuscript

Aram Gurzadyan, Pau Formosa Jordan, Henrik Jönsson (2024)
<i>Regulation and maintenance of polar pattern formation in single cell models</i> (submitted)


## This repository

The main source codes provided in this repository are distributed in the folders associated with individual (sub)figures of the manuscript: All datasets generated on HPC cluster are provided in the corresponding folder, unless otherwise stated.

The numerical simulations, pseudo-analytical analysis and data visualisation were conducted in Wolfram Mathematica 12.0. For specific details on the numerical solvers used please refer to the Methods in the manuscript and to the simulation source codes.  Data parsing and curation was conducted by scripts written in C++11.

Further comments are given in the source files within the respective folders.


## Data

Datasets of high-troughput simulation ensambles and pseudo-analytical parameter space scans obtained on the HPC are provided in the appropriate folder.
The _Computational Analysis Browser for Cell Polarity Reaction-Diffusion Networks_ interactive web platform `https://interactive-platform.gitlab.io/interactive-platform/` visualises our results of parallised simulations and 7D parameter space analysis.


## Contact

For further information or inquiries, please contact

aram.gurzadyan@sluc.cam.ac.uk

henrik.jonsson@slcu.cam.ac.uk
