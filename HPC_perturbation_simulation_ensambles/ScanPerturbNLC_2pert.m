(* ::Package:: *)

(*Limits Mathematica to requested resources*)
Unprotect[$ProcessorCount];$ProcessorCount = 50;
 
(*Prints the machine name that each kernel is running on*)
Print[ParallelEvaluate[$MachineName]];


FsolvePerturbationPersistance[pararray_,filename_,compnum_]:=Module[{ux,vx,uxdata,vxdata,uxdataALL,vxdataALL,steadysoln,L},
f[u_,v_]:=v*(c*u^h/(p^h+u^h))+v*q;
gu[u_,v_]:=f[u,v]-u;
gv[u_,v_]:=-f[u,v]+u;

L=compnum;

total=pararray[[1]];c=pararray[[2]];p=pararray[[3]];h=pararray[[4]];q=pararray[[5]];Du=pararray[[6]];Dv=pararray[[7]];

(*final simulation time*)
tfin=1000;

(*initial conditions*)
steadysoln=N[Quiet[Solve[Rationalize[{gu[u,v]==0,u+v==total,u>=0,v>=0}],{u,v}]]];

(*kick generation in the initial conditions*)
kickstart=50; (*kick starts here*)
kickend=50+pararray[[9]]; (*kick ends here*)
kickstart1=1; (*second kick starts here, if none put the same as for the first kick*)
kickend1=1+pararray[[9]]; (*second kick ends here, if none put the same as for the first kick*)
\[Delta]u=pararray[[8]];(*kick amplitude*)
\[Delta]v=0;(*kick amplitude*)

uxdataALL={};vxdataALL={};
For[hssnum=1,hssnum<=Length[steadysoln],hssnum++,
ind=Table[i-1,{i,L+3}];
ind[[1]]=L;ind[[L+2]]=1;ind[[L+3]]=2;
cc[i_]:=ind[[i+1]];
ux=Table[ToExpression[StringJoin["u",ToString[i]]],{i,1,L}];vx=Table[ToExpression[StringJoin["v",ToString[i]]],{i,1,L}];
usteadysoln=u/.steadysoln[[hssnum]];vsteadysoln=v/.steadysoln[[hssnum]];
Table[ukick[i]=usteadysoln+(If[(kickstart<=i<=kickend||kickstart1<=i<=kickend1)&&\[Delta]u!=0,\[Delta]u-usteadysoln,0]),{i,1,L}];
Table[vkick[i]=vsteadysoln+(If[(kickstart<=i<=kickend||kickstart1<=i<=kickend1)&&\[Delta]v!=0,\[Delta]v-vsteadysoln,0]),{i,1,L}];
evolu[u_,um1_,uM1_,v_]:=gu[u,v]+Du*(uM1+um1-2*u);evolv[u_,v_,vm1_,vM1_]:=gv[u,v]+Dv*(vM1+vm1-2*v);
ss=NDSolve[Flatten[
Table[{ux[[i]]'[t]==evolu[ux[[cc[i]]][t],ux[[cc[i-1]]][t],ux[[cc[i+1]]][t],vx[[i]][t]],
vx[[i]]'[t]==evolv[ux[[cc[i]]][t],vx[[cc[i]]][t],vx[[cc[i-1]]][t],vx[[cc[i+1]]][t]],
ux[[i]][0]==ukick[i](1+0.1 RandomVariate[UniformDistribution[{0,1}],1,WorkingPrecision->10][[1]]),vx[[i]][0]==vkick[i](1+0.1RandomVariate[UniformDistribution[{0,1}],1,WorkingPrecision->10][[1]])},{i,1,L}]],
Flatten[Table[{ux[[i]],vx[[i]]},{i,1,L}]],{t,0,tfin},Method->"StiffnessSwitching"];
jump=10;
uxdata=Table[Flatten[Table[{i,ux[[i]][kk]}/.ss,{i,1,L}],1],{kk,0,tfin,jump}];vxdata=Table[Flatten[Table[{i,vx[[i]][kk]}/.ss,{i,1,L}],1],{kk,0,tfin,jump}];
AppendTo[uxdataALL,uxdata];AppendTo[vxdataALL,vxdata];
];
peakN=ConstantArray[0,Length[steadysoln]];
(minmax=(Max[uxdataALL[[#,tfin/jump,All,2]]]-Min[uxdataALL[[#,tfin/jump,All,2]]])/2;crossedfrombelow=0;
For[i=1,i<=Length[uxdataALL[[#,tfin/jump,All,2]]],i++,
If[uxdataALL[[#,tfin/jump,All,2]][[i]]>minmax&&crossedfrombelow== 0,crossedfrombelow=1];
If[uxdataALL[[#,tfin/jump,All,2]][[i]]<minmax&&crossedfrombelow== 1,crossedfrombelow=0;peakN[[#]]++];
];)&/@Range[1,Length[steadysoln]];
WriteString[OpenAppend[filename],ToString[total]<>","<>ToString[Du]<>","<>ToString[Dv]<>","<>ToString[L]<>","<>ToString[pararray[[9]]]<>","<>ToString[pararray[[8]]]<>","<>ToString[(Ordering[(Abs[Fourier[uxdataALL[[#,tfin/jump,All,2]]-uxdataALL[[#,1,All,2]]]]^2)[[1;;Ceiling[L/2]]],-1]-1)[[1]]&/@Range[1,Length[steadysoln]]]<>","<>ToString[peakN]<>"\n"];
Close[filename];
]


global = {3,3,2,0};

argTOTAL = {3.6,3.7,3.8,3.9,4};
argDIFF1 = {0.01,0.05,0.1,0.5,1,5,10,50};
argDIFF2 = {10,50,100,500,5000000};
argL = {100};

argDELTA = {2,5,10,20,30,40};
argWIDTH = {0,4,9,19,39};


(*Define the stream into the output file*)
filename="/home/ag979/PerturbPersist/NLC1_L100_2perturb_persist"<>DateString["ISODateTime"]<>".txt";
If[FileExistsQ[filename],DeleteFile[filename]];
CreateFile[filename];
WriteString[OpenAppend[filename],"2kick. State parameters {c,p,h,q} = "<>ToString[global[[1;;4]]]<>"\n"];
Close[filename];


(*LoopRun*)
Print["The parallel code has started at "<>DateString["ISODateTime"]<>".\n"];
ParallelDo[
FsolvePerturbationPersistance[{iargTOTAL,global[[1]],global[[2]],global[[3]],global[[4]],iargDIFF1,iargDIFF2,iargDELTA,iargWIDTH},filename,iargL],
{iargTOTAL,argTOTAL},
{iargDIFF1,argDIFF1},
{iargDIFF2,argDIFF2},
{iargL,argL},
{iargDELTA,argDELTA},
{iargWIDTH,argWIDTH}
];
Print["The parallel code has finished at "<>DateString["ISODateTime"]<>".\n"];
